/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul7;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 *
 * @author D205
 */
public class contohGui extends JFrame {
    public contohGui() {
        super("Contoh GUI"); // Title
        setSize(500, 300); // Set ukuran
        
        JLabel lbl = new JLabel("Gui java sangat menyenangkan"); // label
        
        JPanel panel = new JPanel(); // membuat panel baru
        panel.add(lbl); // memasukan label kedalam panel
        getContentPane().add("North", panel); // menaruh panel pada posisi atas
        
        this.show();
    }
    
    public static void main(String[] args) {
        new contohGui();
    } 
}

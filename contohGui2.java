/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul7;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author D205
 */
public class contohGui2 extends JFrame{
    public contohGui2() {
        super("Contoh 2 GUI"); // title
        setSize(500, 300); // set ukuran
        
        JButton tbl = new JButton("Saya siap!!!!");// membuat panel baru
        
         JPanel panel = new JPanel(); // membuat panel baru
         panel.add(tbl); // memasukkan label kedalam panel 
         getContentPane().add("North", panel); // menaruh panel pada posisi atas
         
         // aksi event
         tbl.addActionListener(new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent evt) {
                 System.out.println("Saya menekan tombol dan siap untuk java");
             }
         });
         
         this.show();
    }
    public static void main(String[] args) {
        new contohGui2();
    }
}

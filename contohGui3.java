/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul7;

import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *
 * @author D205
 */
public class contohGui3 extends JFrame{
    JTextField str;
    public contohGui3() {
        super("Contoh 2 GUI"); // title
        setSize(500, 300); // set ukuran
        
        JButton tbl = new JButton("Saya siap!!!!");// membuat panel baru
        str = new JTextField();// create box
        str.setPreferredSize(new Dimension(100, 20)); // create textbox
        
        JPanel panel = new JPanel(); // membuat panel baru
        panel.add(tbl); // memasukan tombol
        panel.add(str); // memasukan text
        getContentPane().add("West", panel); // menaruh panel
        
         // aksi event
         tbl.addActionListener(new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent evt) {
                 System.out.println("yang saya inputkan adalah" + str.getText());
             }
         });
         this.show();
    }
    
    public static void main(String[] args) {
        new contohGui3();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul7;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 *
 * @author D205
 */
public class contohGui4 extends JFrame{
    JComboBox cBox; 
    public contohGui4() {
         super("Contoh 2 GUI"); // title
         setSize(300, 80); // set ukuran
         
         cBox = new JComboBox(new DefaultComboBoxModel(new String[] {"Merah", "Kuning",  "Biru"})); // create combobox
         
          JPanel panel = new JPanel(); // membuat panel baru
          panel.add(cBox);  // memasukan box dalam panel
          getContentPane().add("West", panel); // menaruh panel pada posisi atas
          
          // aksi event
         cBox.addActionListener(new java.awt.event.ActionListener() {
             public void actionPerformed(java.awt.event.ActionEvent evt) {
                 System.out.println("yang saya inputkan adalah index " + cBox.getSelectedIndex());
             }
         });
         this.show();
    }
      public static void main(String[] args) {
        new contohGui4();
    }
}
